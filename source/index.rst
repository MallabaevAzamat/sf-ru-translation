.. Starfinder Core Rulebook documentation master file, created by
   sphinx-quickstart on Wed Sep 18 11:06:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

| **СООБЩЕСТВО**
| Если вы хотите поддержать проект, то можете найти информацию на странице :doc:`support`.


Pathfinder 2E
======================================================

.. toctree::
   :maxdepth: 2
   
   chapter_2/additions


.. toctree::
   :maxdepth: 1

   licenses
   support
   changelog
